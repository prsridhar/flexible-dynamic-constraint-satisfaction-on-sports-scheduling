


CSE 537: PROJECT WORK  
Fair Fixture Scheduling using Flexible and Dynamic Constraint 
Satisfaction
Gangabarani Balakrishnan, Prasanth Sridhar, Gayathri Shivakumar
Group 4


Abstract
Constraint satisfaction is an interesting concept which has been consistently explored and applied in solving graph and scheduling problems. However, there are limitations in the application of classical constraints over real world problems. The classical CSP approach is too rigid to solve scheduling algorithms in practice that expect some relaxation over the constraints. Moreover, classical constraint satisfaction approach tends to resolve the problem from scratch whenever new constraints are added. Hence it does not work well on real-time applications that tend to add dynamic constraints and expect instant solutions. The scope of this project is to apply the variants of the constraint satisfaction such as flexible constraint satisfaction [1] and dynamic constraint satisfaction [1] to scheduling problems in practice (sports matches scheduling). As far as enforcing relaxation on constraints is concerned, flexible constraint satisfaction principles were explored and applied to solve the home/away neutrality problem in sports scheduling applications. Next, dynamic constraint satisfaction principles were applied to impose further constraints on the obtained solution without resolving the entire problem from scratch. Also, a few other practical constraints related to the sports domain were explored which led to a handful of interesting findings and future score.

 Introduction 
	The problem at hand involves scheduling fixtures for any sports competition involving teams that face each other twice a season as home and away fixtures. The scheduling is synonymous to a round robin fashion. This system is analogous to most of the sports leagues in the real world. The constraint satisfaction solves a feasibility problem by assigning variable to values in a given domain based on the constraints placed on the variable. Here, in the fixture scheduling problem, the matches are the variables and the domain consists of the teams available to play those matches. The basic constraints are similar to most of the sports leagues present, where a single team can play only once in a given week and two teams cannot play more than once in a given venue. Our application takes on the scheduling problem by enforcing these basic constraints and applying flexible and dynamic constraint satisfaction techniques on top of them.
Problem Definition

Problem: Given a list of teams, their home venues and their star ratings, employ a scheduler with an algorithm that is fair to all the teams neutralizing the home/away advantages. Also scheduling preference has to be aligned with TV audience where the star teams are made to be played during times of peak viewership, also providing no hindrance to the prior mentioned primary problem of home/away neutrality.
The schedule generated also has to be flexible enough capable of accepting last minute dynamic constraint requests to change match schedule in real-time.

The primary problem on home/away neutrality is solved using flexible constraint satisfaction approach of regulated weights. The secondary problem on TV viewership preference was tackled using a divide and conquer approach. The other secondary problem on handling real-time requests is solved using dynamic constraint satisfaction approach that employs a simple local repair algorithm.
 
The algorithm attempts to solve this fixture scheduling-  problem, using the following data, the input for the problem consists of the number of teams, team names weights associated with the teams based on fan base, their respective stadiums and the city where the teams are from. The system is also capable of handling dynamic inputs in real-time that can enforce additional constraints placed on fixtures such as the "team A should play Team B" at a given time slot on a given weekend or "team A shouldn't play Team B" on a given weekend. Inputs fed to the system are file-based inputs.
	
The output of the problem will contain list of scheduled matches on a GUI as well as on an output file. The output data will be formatted to display the list of games scheduled on a weekly basis, the output will display the time the game starts, venue and whether the given game is a "Derby", which denotes a game between fierce rivals/teams which are located in the same city. The match schedule on a given week will be based on the weight of the fan base and unique slots assigned based on the fans base.

Motivation
	Fixture Scheduling is a real world problem where a certain degree of fairness has to be exercised to give equal opportunity to all the teams. This is especially the case involving leagues which play home and away fixtures, where the team playing at home has a considerable advantage in practice. To schedule a long of list of matches away from home might adversely affect a team's morale and affect their performance. Popular sports leagues are multi-billion-dollar business where such partiality will not be tolerated. Our Scheduling Algorithm tries to solve this problem by alternating home and away fixtures in the fairest manner possible, we were able to limit the number of consecutive home and away fixtures to a minimum using Flexible CSP. 
	The matches consisting of teams having the largest fan base get unique time slots to maximize the TV coverage. The TV deals these days are multibillion dollar contracts for popular sports leagues. In our example, we have chosen to emulate English premier league whose TV deal last year was worth more than eight billion pounds [3] in revenue. For such a high fee, the broadcaster has the right to modify the schedule based on the match fan base to maximize the number of viewers. This can be done by adding additional constraints placed on a given match, choosing when it should be played. This is accomplished in our application using dynamic CSP. 
	Our application will schedule matches based on the popularity of the team playing in it.  As of now, we are manually adding weightage to teams based on fan base. One can easily extend this to assign weightage using data from twitter or any other social media presence of a team. One more possible extension is assigning time slots to matches based on the geography of the fan base. Given the popularity of the teams are worldwide, one can schedule matches for these teams based on the preferred time zone, where their fan base is concentrated.
	
Contribution
		As part of this project we have developed a mock up scheduling application. We have used the constraint satisfaction approach to tackle this scheduling problem and have also applied advanced techniques like Flexible CSP to improve the fairness in fixture scheduling and dynamic CSP to give more fine grain control over fixtures at real-time. We have also developed a GUI to give a mockup feel to the project.
Our project is a hybrid of employing advanced techniques and Application. 
Learning Outcomes
	The knowledge on Constraint Satisfaction was developed as part of the curriculum of this AI course. Through this project we were able to ascertain the limitations of the classical constraint satisfaction approach in dealing with some real world problems at practice.  We had to explore, learn and apply variants of constraint satisfaction like Flexible CSP and Dynamic CSP and understand the practical implications of those approaches. During the course of this project, we were able to deeply understand some nuances in fixture scheduling like integer programming and constraint programming. The references found on these variant CSP topics were mostly theoretical. We had to overcome the challenge of bridging those concepts in theory to practice through brain-storm sessions while developing this application.
Description
Our model described considers 20 teams which compete against each other over 38 weeks in fixtures scheduled in 6 time slots over the weekend. Each time slot, 12 noon, 2 p.m and 4 p.m can have three matches. We hit off by taking into account the two mandatory rigid constraints:
1.	Any team cannot play more than one match during a single weekend
2.	Two teams cannot be pitted against each other more than one once at a given venue
These constraints form the very basis of our formulation of a Constraint Satisfaction Problem. A traditional solution using solution formulation would lead us to a satisfactory solution in a reasonable amount of time. But, the assumption of only these two constraints is a far cry from the real-world scenario. It is here that the rest of the constraints which play a key role in the world of fixture scheduling venture. We have many classic constraints like: 

1.	The “Home-Away” problem: “Home” match is a match which is played in the geographical location from where a team belongs. A match played in any other location is termed as an “Away” match. Any team which plays a home match has a huge favor owing to the home crowd support. Continuous scheduling of home matches leads to a moral boost to any team, thus putting the fairness of the league in jeopardy. To cut a square deal, ideally every team should be playing an “away” match after a “home” match. Making this consideration leads to a “No Solution” state with the traditional constraint satisfaction approach. 
2.	New Additional Constraints: There might be numerous situations in which we are bound to add on new constraints. For instance, there might be unfavorable weather conditions which might add a constraint to the schedule, thus eliminating a particular venue from the available list of options. This situation demands a re-schedule of the whole fixture list, thus incurring huge re-work. The additional constraint, after addition, requires the computation of thee schedule right from the beginning again.
3.	Equal Exposure: Any team pair cannot encounter each other until and unless they have faced the rest of the opponents to give an equal opportunity to every team to face off against every potential opponent. This condition, if accommodated, in the traditional constraint satisfaction problem, might lead to a situation with no solution, thus leaving us with no proper schedule.
Our model aims at overcoming these cons of the traditional constraint satisfaction problem by incorporating novel methods along with the traditional approaches. We solve the above-mentioned problems using:

1.	Flexible Constraint Satisfaction: This is the key to the “Home-Away” problem. Here, we tend to relax the constraint of alternate home-away matches in order to reach a reasonable solution. We trade-off the hard and fast rule of alternation with a reasonable solution where we reduce the chance of consecutive home matches by a huge margin. We use a “Weighting” mechanism to weigh the teams on the basis of the number of home matches they’ve been involved in. Initially, half the teams play with the home advantage and hence, are weighed more compared to their counterparts who play an away match. When scheduling the next match, this weight is taken into consideration and the home ground of the teams with less weight are given more priority to be picked as a venue. This way, though we don’t achieve complete alternation, we avoid lack of solution and reach to a reasonable situation of balance. 
2.	Dynamic Constraint Addition: This approach is weaved into our solution for achieving maximum emulation of the real-time scenario. Any additional constraints, be it elimination of a venue or withdrawal of a team from the league, can be included into our model at any time without having to draft the schedule from the scratch. For instance, in our model we consider two dynamic constraints stating that a team pair must or mustn’t play in a particular week. The classical approach requires huge re-work as the schedule is already built. We introduce the “Local Search” approach here. We take the current existing schedule and consider the additional constraint. We search for the inconsistency and do a local search to find a better solution which will remove the inconsistency incurred, that is, we search for a better solution. With this approach, we avoid a potential “No Solution” state of the traditional approach.
3.	Reverse Approach: For the equal exposure of teams, we abide by the reverse approach technique where we split the existing set of teams into 2 halves. Once we schedule the matches for the first set of teams, we reverse the fixtures in order to ensure that each of the team places a home and a away match respectively. This, not only reduces the size of problem by 50%, but also helps us in reducing the home-away issue to some extent. 

Besides, we have also implemented a few additional features which, we feel, help us in emulating the real-world scenario in a better fashion. 

1.	Maximization of Television Revenue: Every league owes its success to the fan-base. We tried to include this factor while scheduling the matches. By using the “weighting” concept, the fan-base factor of a team pair is found. This fan-base factor helps in scheduling the matches with higher value in “Unique” slots. Unique slots are the time slots in which there is just one match. Sundays are the days which are given more priority. So we sort the team-pairs based on the fan-base factor. The top 3 teams are allotted the slots on Sunday. The next three teams are allotted slots on Saturday. The rest of the 4 teams are spread out on Saturday, ensuring that the team pairs with maximum fan base have the match scheduled in a unique slot on Sunday.

This ensures that the television channel airing these matches collects the maximum revenue, considering the increase in the viewership because of our schedule. 

2.	Derby Matches: A sporting fixture between two teams from the same town, city or region, is called a Derby. Any match involving fierce rivalry is also termed as Derby. We have represented this factor in our schedule to draw the attention towards matches which will definitely grab most of the eyeballs round the world. 

Future Scope 

We intend to make further enhancements to our current system based on the new features we have incorporated. 
Maximization of Television Revenue can be increased further by scheduling the matches according to the regional times. We plan to take into consideration the fan-base factor and schedule the matches according to the regions where the fan-base is concentrated. Furthermore, derby matches are another crucial area where the revenue of televisions can be maximized. Any derby match would pull in a lot of viewers and thus increase the viewership exponentially leading to huge profits.
Evaluation
Analysis on Flexible CSP
		This data does not require handling of any large datasets as the goal was to explore the constraint satisfaction techniques over a real word problem at practice. Hence the data is fed to the system as file inputs. The system uses a variant on classical CSP to solve the home/away neutrality problem in scheduling. Our variant CSP is flexible enough capable of relaxing the constraints based on need. When the system employed comes to realization that it is strangled with tight constraints and the constraints are too rigid to derive a feasible solution for the problem, the system adapts to relax the constraints based on priority at that point of time. Once it gets out of the tightly constrained point, it again reverts back tightening its relaxed constraints. This flexibility based on the constraint environment is not available to a system employed with classical CSP approach. Evaluation is done between the base and variant system using comparison of raw-outputs. 
	For the purpose of empirical analysis based on comparison, we also developed a system that employs classical CSP. The basic constraints as mentioned earlier were enforced. Also the constraint on home/away neutrality were made rigid such that each team must play a home match after every away match and vice versa. This system uses a depth first search with backtracking, a classical CSP algorithm. The system is fed with an input team-size of 4 teams. The system was successfully able to generate a match schedule also enforcing the home/away neutrality constraint. 
Team Count = 2
Scheduling raw output,

[(0, 1)]

[(1, 0)]

Raw output format: Each row specifies matches scheduled for a week. Each tuple represents a match. The first value in each tuple represents home team and second value represents away time. The output obtained shows that classical CSP was able to solve the home/away neutrality problem.
	This might be due to the fact that the data size input was too small to expose the vulnerability of the system. However, when the very same system was fed with real-world data where the team-size is 4, the system failed to come up with the solution. Thus, the very same problem statement has become non-solvable for inputs of team count 4 when the system was employed with only classical CSP. We can see from the below pic that the system was able to solve only till match fixture week 8 when team count was given as 4.
Team count = 4
Scheduling raw output,

[(0, 1), (2, 3)]

[(1, 2), (3, 0)]

[]

[]

	The raw output depicts that the classical CSP was provide match fixtures only till Week 8.
	Now the same problem that derailed the system with classical CSP is fed to our system that has enforced flexible CSP with regulated weightage scores on home/away for every team. The system was able to generate a match schedule of fixtures for this problem also ensuring that the home/away neutrality problem is solved with optimality. We could clearly see home/away repeats on teams where a team is made to play two consecutive home games or away games. This is the point where the classical CSP system gets strangled by the tight constraints and fails to agree on a solution as it is devoid of assignments. But our system with flexible CSP adapts in this situation of a tightly constrained environment and relaxes the constraints allowing a team to play consecutive home/away games. However, after countering the situation it quickly tightens back the relaxed constraint by making the necessary changes to the regulation weights such that the team that played consecutive away games is given the highest priority to play the home game during the next round. This ensures the system is capable of arriving at a solution providing optimal alignment to the home/away neutrality constraint.


Team Count : 20 
[(0, 1), (2, 3), (4, 5), (6, 7), (8, 9), (10, 11), (12, 13), (14, 15), (16, 17), (18, 19)]

[(1, 2), (3, 0), (5, 6), (7, 4), (9, 10), (11, 8), (13, 14), (15, 12), (17, 18), (19, 16)]

[(0, 5), (2, 7), (4, 1), (6, 3), (8, 13), (10, 15), (12, 9), (14, 11), (16, 18), (17, 19)]

[(18, 0), (1, 17), (3, 4), (5, 2), (7, 8), (9, 6), (11, 12), (13, 10), (15, 16), (19, 14)]

[(0, 7), (2, 9), (4, 11), (6, 1), (8, 3), (10, 5), (12, 18), (14, 16), (17, 13), (15, 19)]

[(16, 0), (1, 15), (3, 10), (5, 8), (7, 12), (9, 4), (11, 2), (13, 18), (19, 6), (14, 17)]

[(18, 14), (0, 9), (2, 16), (4, 19), (6, 11), (8, 1), (10, 7), (12, 3), (13, 5), (15, 17)]

[(17, 0), (1, 10), (3, 13), (5, 12), (7, 15), (9, 18), (11, 16), (14, 2), (19, 8), (4, 6)]

[(16, 4), (0, 11), (2, 17), (6, 14), (8, 10), (12, 1), (13, 7), (15, 3), (18, 5), (9, 19)]

[(10, 0), (1, 9), (3, 16), (4, 2), (5, 15), (7, 18), (11, 13), (14, 8), (17, 6), (19, 12)]

[(0, 4), (2, 10), (6, 8), (9, 3), (12, 14), (13, 1), (15, 18), (16, 5), (7, 17), (11, 19)]

[(8, 0), (18, 11), (1, 7), (3, 5), (4, 12), (10, 14), (15, 2), (17, 9), (19, 13), (6, 16)]

[(5, 1), (14, 3), (0, 6), (2, 8), (7, 19), (9, 15), (10, 18), (11, 17), (12, 16), (13, 4)]

[(16, 7), (1, 11), (3, 17), (4, 14), (6, 2), (8, 18), (10, 12), (15, 0), (19, 5), (9, 13)]

[(17, 4), (18, 1), (0, 19), (2, 12), (5, 9), (7, 14), (8, 15), (11, 3), (13, 6), (10, 16)]

[(12, 0), (14, 5), (1, 3), (4, 10), (6, 18), (7, 11), (9, 16), (15, 13), (19, 2), (8, 17)]

[(3, 7), (16, 8), (0, 13), (2, 18), (5, 11), (10, 6), (17, 12), (1, 19), (4, 15), (9, 14)]

[(11, 9), (13, 2), (18, 4), (0, 14), (6, 15), (7, 5), (8, 12), (19, 3), (1, 16), (10, 17)]

[(12, 6), (14, 1), (15, 11), (2, 0), (3, 18), (4, 8), (5, 17), (9, 7), (16, 13), (19, 10)]

	The raw-out depicts Flexible CSP system was able to solve for team count input of 20 by relaxing the constraints based on necessity.
Analysis on Dynamic CSP
	Besides the flexible CSP which is the focal point of this project, additional variant techniques on CSP like Dynamic CSP were also explored. This approach was applied in our system so that the system is capable of modelling the existing solution based on real-time input constraints instead of resolving the entire solution from scratch. This is bound to improve the performance of a system. This approach uses a concept of local repair on the existing solution. The time taken to remodel an existing solution is compared over the time taken to solve an entire solution from scratch. The performance comparison was done using simple stopwatch process that uses time package of python. 
Time taken to schedule from scratch = 50.3ms
Time taken to remodel existing schedule using = 11.4ms 
	There are two dynamic constraints that could be fed to our system. They are the “ShouldPlay” and “ShouldNotPlay” constraints. The repair algorithm devised for “ShouldNotPlay” constraint works to perfection. Although an efficient algorithm was defined for the “ShouldPlay” algorithm, it fails to perform the repair on the following corner cases, 
When two matches belonging to different weeks in existing solution is given as a “ShouldPlay” constraint for a single week.
When two matches belonging to a single week in existing solution is given as “ShouldPlay” constraints for different weeks
Conclusion
	We set out on modelling a solution to the fixture scheduling problem. Though a traditional solution based on constraint satisfaction exists, it has many shortcomings including incompatibility to the “home-away” problem, lack of convenience for including dynamic constraints and failure in fulfilling the equal exposure requirement. Using many novel approaches like Flexible constraints, Dynamic constraint handling and Reverse approach, we could solve these issues reasonably. Also, we included new features like weighting the fan base to understand the importance of a match and schedule them accordingly in a unique fashion. Moreover, we have highlighted derby matches as well which we plan to use as a feature to increase the television viewership and revenue in the future.


References
Classical Constraint Satisfaction slides by Dr. Niranjan Balasubramaniam, Assistant Professor, StonyBrook University.
“Dynamic flexible constraint satisfaction and its application to AI planning”, thesis paper by Ian Miguel
“Sports Scheduling: Algorithms and Applications” by Scott Anson & Sam Lester
http://www.telegraph.co.uk/sport/football/12141415/Premier-League-clubs-to-share-8.3-billion-TV-windfall.html

